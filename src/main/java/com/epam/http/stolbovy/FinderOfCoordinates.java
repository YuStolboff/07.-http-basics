package com.epam.http.stolbovy;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FinderOfCoordinates {

    private static final int NUMBER_OF_REQUEST = 2;
    private static final Logger log = LoggerFactory.getLogger(FinderOfCoordinates.class);

    public static void main(String[] args) {
        final String regexpToken = "\"csrfToken\":\"[^\\\"]++\"";
        final String regexpYandexuid = "yandexuid=[^\\\"]++\"";
        final String regexpCoordinates = "\"coordinates\":[^\\\"]++";
        final String address = "Ижевск Труда 66";
        String token;
        String yandexuid;
        String lineResponse = null;
        FinderOfCoordinates finderOfCoordinates = new FinderOfCoordinates();
        URIBuilder uriBuilder = new URIBuilder();
        uriBuilder.setScheme("https").setHost("yandex.ru").setPath("/maps/44/izhevsk/");
        try {
            log.info("URI: {}", uriBuilder.build());
        } catch (URISyntaxException e) {
            log.info("URISyntaxException: {}", e);
        }
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            for (int i = 0; i < NUMBER_OF_REQUEST; i++) {
                HttpGet httpGet = new HttpGet(uriBuilder.build());
                CloseableHttpResponse response = httpClient.execute(httpGet);
                log.info("Status: {}", response.getStatusLine());
                HttpEntity httpEntity = response.getEntity();

                try (BufferedReader bufferedReader = new BufferedReader(
                        new InputStreamReader(httpEntity.getContent(), Charset.forName("UTF-8")))) {
                    lineResponse = bufferedReader.readLine();
                    log.info("Response: {}", lineResponse);
                    token = finderOfCoordinates.findExpression(lineResponse, regexpToken);
                    yandexuid = finderOfCoordinates.findExpression(lineResponse, regexpYandexuid);
                    log.info("token: {}", token);
                    log.info("yandexuid: {}", yandexuid);
                }
                uriBuilder.setPath("/maps/api/search").setParameter("text", address)
                        .setParameter("csrfToken", token)
                        .setParameter("lang", "ru_RU");
            }
            log.info("URI: {}", uriBuilder);
            String coordinates = finderOfCoordinates.findExpression(lineResponse, regexpCoordinates);
            log.info("Coordinates: {}", coordinates);
        } catch (IOException | URISyntaxException e) {
            log.info("Exception: {}", e);
        }
    }

    private String findExpression(String response, String regexp) {
        Pattern pattern = Pattern.compile(regexp);
        Matcher matcher = pattern.matcher(response);
        if (matcher.find()) {
            String token = matcher.group();
            token = token.replaceAll("\"", "").replaceFirst(":", "=");
            token = token.substring(token.lastIndexOf("=") + 1);
            return token;
        }
        return "";
    }
}